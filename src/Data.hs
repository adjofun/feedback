{-# LANGUAGE TemplateHaskell #-}

module Data (
  Data
, Distribution
, Freq
, FreqRank (..)

, getData

, distribution
, frequencies
, mean
, stdDev
, func
, task
, freq
, rank
) where


import qualified Data.ByteString.Lazy as BL
import qualified Data.Vector as V
import qualified Data.Csv as CSV
import qualified System.IO.Error as IOE

import Control.Lens
import qualified Data.Random.Distribution as RD
import qualified Data.Random.Distribution.Normal as RD

import Presets


data Data = Data
  { _distribution :: Data.Distribution
  , _frequencies :: [Freq]
  }

data Distribution = Distribution 
  { _mean :: Double
  , _stdDev :: Double
  , _func :: Double -> Double
  }

data FreqRank = Low | Medium | High deriving (Eq, Show)

data Freq = Freq
  { _task :: Int
  , _freq :: Double
  , _rank :: FreqRank
  }

newtype Raw = Raw (V.Vector (V.Vector Int))


makeLenses ''Data
makeLenses ''Distribution
makeLenses ''Freq


getData :: FilePath
        -> Preset
        -> IO (Either String Data)
getData dataFile preset = do
  raw <- getRaw dataFile
  return $ do
    dataVector <- raw
    Right $ Data
           (raw2distr dataVector preset)
           (raw2freqs dataVector preset)


getRaw :: FilePath
       -> IO (Either String Raw)
getRaw dataFile = (flip IOE.catchIOError) (return . Left . show) $ do
  byteString <- BL.readFile dataFile
  return $ do
    raw <- CSV.decode CSV.NoHeader byteString
    Right $ Raw raw


raw2distr :: Raw
          -> Preset
          -> Distribution
raw2distr (Raw dataVector) preset =
  let totalScores = V.map V.sum dataVector

      normMean = fromIntegral (V.sum totalScores)
           / fromIntegral (V.length totalScores)
      normStdDev = sqrt $ (1 / fromIntegral (V.length totalScores))
                        * V.sum (V.map (\x -> (fromIntegral x - normMean) ** 2) totalScores)

      distr = RD.Normal normMean normStdDev
      a = 0
      b = fromIntegral (preset^.exam.totalScore)
      truncNorm x
        | normStdDev /= 0 =  RD.pdf distr x
                          / (RD.cdf distr b - RD.cdf distr a)
        | x == normMean   = 1
        | otherwise       = 0

      s = sum $ map truncNorm [a..b]
      diff = 1 - s
      fixedTruncNorm x
        | x >= a && x <= b = truncNorm x + ((diff*truncNorm x)/s)
        | otherwise = 0

  in Distribution normMean normStdDev fixedTruncNorm


raw2freqs :: Raw
          -> Preset
          -> [Freq]
raw2freqs (Raw dataVector) preset =
  let l = V.length (dataVector V.! 0)
      tasks = V.map (\i -> (i, V.map (V.! i) dataVector)) (V.fromList [0..l-1])
      toFreq (index, taskScores) =
        let taskPreset = (preset^.maxTaskScores) !! index 
            taskFreq = fromIntegral (V.sum taskScores) 
                     / fromIntegral (V.length taskScores)
                     / fromIntegral (taskPreset^.maxScore)
            taskRank
              | taskFreq < (taskPreset ^. lowFreq) = Low
              | taskFreq >= (taskPreset ^. highFreq) = High
              | otherwise = Medium
        in Freq (index+1) taskFreq taskRank
  in V.toList $ V.map toFreq tasks
